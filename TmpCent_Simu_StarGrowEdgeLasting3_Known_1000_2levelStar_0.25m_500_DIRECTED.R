library( parallel)
library( snow, lib.loc = "~/Rpackages")
library( Rmpi, lib.loc = "~/Rpackages")

library( igraph, lib.loc = "~/Rpackages")


source("/home/hanke/Programme/Tmp-Cent/Funktionen/Funktion_t_closeness.R")
source("/home/hanke/Programme/Tmp-Cent/Funktionen/Funktion_k-Schritte-pro-Intervall.R")
source("/home/hanke/Programme/Tmp-Cent/Funktionen/Funktion_bloat.edgestream.R")
source("/home/hanke/Programme/Tmp-Cent/Funktionen/Sterngraphen.R")



### Create cluster and export required objects
mc<-250#Optional: number of cores can be determined automatically
cl <- makeCluster(mc, type="MPI")

TypeOfSimulation <- "Growng Star with 3 Timesteps lasting Edges and Noise;
                    Known: number of timesteps till first snapshots are known"
NSimu <- 500

clusterExport(cl, c("bloat.edgestream","intervalle.k","t_closeness", "erdos.renyi.game", 
                    "graph.union", "get.edgelist", "NSimu", "E", "degree", "centr_clo",
                    "sterngraph_edgelist", "graph_from_edgelist", "NSimu", "TypeOfSimulation"))

TmpCent_Simu_StarGrowEdgeLasting3_Known_1000_2levelStar_0.25m_500_DIRECTED <- parLapplyLB(cl=cl, 1:NSimu, fun=function(x){
  
  
  ### Art der Simulation ###
  #~~~~~~~~~~~~~~~~~~~~~~~~#
  # Wachsender Sterngraph mit 3 Schritten haltenden Sternkanten und Rauschen
  
  # Anzahl Schritte
  K=1
  # Anzahl Knoten
  numV <-1000
  # Wie hoch soll der Anteil an Kanten im Noisegraphen im Verh?ltnis zum
  # Sterngraphen sein
  AnteilKanten <-0.25
  # Um welchen Faktor soll die Anzahl an Zeitpuntken numT die Anzahl an
  # Levels des Sterngraphen ?bersteigen
  FaktorTimepoints <- 2
  # nach wievielen Zeitschritten sollen die Kanten vom Stern kollabieren:
  StarEdgesLasting <- 3
  
  start.seed <-x+54001 
  set.seed(start.seed)
  
  # Simulation des Sterns
  Stern <- sterngraph_edgelist(20, neighbours_start = c(10:20), 
                               neighbours_rest = c(0:3), one_list=FALSE,
                               cut_node=1000)
  
  # Wieviele Level, d.h. wie viele Generationen werden so erzeugt
  levelStern <- length(Stern)
  numT <- levelStern*FaktorTimepoints
  start.seed <-x+54002 
  set.seed(start.seed)
  
  # Wie viele Snapshots
  nSnapshots <- sample(floor(numT/4):(numT/2),1)
  
  # Anzahl an Kanten im Noisegraphen
  m <- floor((max(Stern[[length(Stern)]][,2])-1)*AnteilKanten)
  
  
  Gs.sterngrow <- vector(mode="list", length=numT)
  nEdgesTotal <- vector(mode="list", length=numT)
  summaryDegree <- vector(mode="list", length=numT)
  sdDegreeTotal <- vector(mode="list", length=numT)
  closenessStatic <- vector(mode="list", length=numT)
  
  start.seed <-x+54003
  set.seed(start.seed)
  Gs.sterngrow[[1]] <- erdos.renyi.game(numV, m, "gnm")
  ### um zu verhindern, dass der Stargraph leer ist
  while(length(E(Gs.sterngrow[[1]]))<1){
    start.seed <- start.seed+1
    set.seed(start.seed)
    Gs.sterngrow[[1]] <- erdos.renyi.game(numV, m, "gnm")
    
  }
  
  
  StartSternTimepoint <- sample(1:(numT-levelStern+1),1)
  
  
  for(i in 2:numT){
    set.seed(x+i*i+i*123000)
    Gs.sterngrow[[i]] <- erdos.renyi.game(numV, m, "gnm")
  }
  
  nEdgesStar <- lapply(seq_along(Stern), FUN=function(f){
    max(Stern[[f]][,2])-1
  })  
  
  sternindex <- 0
  
  ### da jede Kante des sich entwickelnden Sterns StarEdgeLast lange halten soll,
  # m?ssen F?lle unterschieden werden, an denen u.a. noch nicht StarEdgeLast
  # Schritte in der Sternentwicklung bisher erfolgt sind sowie die F?lle,
  # bei denen die Kanten nur noch "nachhallen", d.h. die Entwicklung des Sterns
  # Schon vorbei ist, es aber noch Kanten von ihm gibt
  
  ### der Vektor der for SChleife ist so kompliziert, weil es maximal bis numT
  # f?r den Nachhall gehen soll
  for(i in (StartSternTimepoint:(StartSternTimepoint+
                                   levelStern+StarEdgesLasting-2))[(StartSternTimepoint:
                                                                      (StartSternTimepoint+
                                                                         levelStern+StarEdgesLasting-2))%in%1:numT]){
    
    sternindex <- sternindex+1
    if((sternindex <= StarEdgesLasting) & (sternindex <= length(Stern))){
      Gs.sterngrow[[i]] <- graph.union(Gs.sterngrow[[i]],
                                       lapply(Stern[1:sternindex],
                                              graph_from_edgelist,FALSE))
      
      
    }
    
    if((sternindex <= StarEdgesLasting) & (sternindex > length(Stern))){
      Gs.sterngrow[[i]] <- graph.union(Gs.sterngrow[[i]],
                                       lapply(Stern[1:length(Stern)],
                                              graph_from_edgelist,FALSE))
      
      
    }
    
    if((sternindex > StarEdgesLasting) & (sternindex <= length(Stern))){
      Gs.sterngrow[[i]] <- graph.union(Gs.sterngrow[[i]],
                                       lapply(Stern[(sternindex - StarEdgesLasting+1):sternindex],
                                              graph_from_edgelist,FALSE))
      
      
    }
    
    if((sternindex > StarEdgesLasting) & (sternindex > length(Stern)) & 
         (sternindex -StarEdgesLasting+1 <= length(Stern))){
      Gs.sterngrow[[i]] <- graph.union(Gs.sterngrow[[i]],
                                       lapply(Stern[(sternindex - StarEdgesLasting+1):length(Stern)],
                                              graph_from_edgelist,FALSE))
      
      
    }
    
  }
  
  
  nEdgesTotal[[1]] <- length(E(Gs.sterngrow[[1]]))
  summaryDegree[[1]] <- summary(degree(Gs.sterngrow[[1]]))
  sdDegreeTotal[[1]] <- sd(degree(Gs.sterngrow[[1]]))
  closenessStatic[[1]] <- centr_clo(Gs.sterngrow[[1]])
  for(i in 2:length(Gs.sterngrow)){
    nEdgesTotal[[i]] <- length(E(Gs.sterngrow[[i]]))
    summaryDegree[[i]] <- summary(degree(Gs.sterngrow[[i]]))
    sdDegreeTotal[[i]] <- sd(degree(Gs.sterngrow[[i]]))
    closenessStatic[[i]] <- centr_clo(Gs.sterngrow[[i]])
  }
  
  ClosenessStar <- 
    centr_clo(graph.union(lapply(Stern, graph_from_edgelist, directed=FALSE)))
  
  Gs <- Gs.sterngrow
  #~~~~~~~~~~~~~~~~~~~~~~~~#
  
  
  set.seed(x)
  zeiten <- sort(sample(1:numT, nSnapshots, replace=FALSE))
  ### da es passieren kann, dass alle Snapshots au?erhalb des Intervalls,
  # in dem der Stern exisitiert, liegen, m?ssen so lange neue Zeiten
  # gezogen werden, bis mindestens ein Snapshot einmal den Stern beinhaltet
  while(!any(zeiten  %in% StartSternTimepoint:(StartSternTimepoint+levelStern-1))){
    x <- x+12749
    set.seed(x)
    zeiten <- sort(sample(1:numT, nSnapshots, replace=FALSE))
  }
  
  Gs.edgelists <- lapply(zeiten, FUN=function(x){
    E1 =get.edgelist(Gs[[x]])
    ### Hier m?ssen die Kanten jetzt noch verdoppelt werden, 
    # sodass neben A-B auch B-A auftaucht!
    #E2 = rbind(E1, E1[ ,c(2,1)])
  })
  
  first <- zeiten[1]
  # K wird angepasst
  if(length(zeiten)>1){
    K <- min(diff(sort(zeiten)))
  }else{
    K <- zeiten[1]
  }
  
  steps_allowed <- intervalle.k(zeiten, first ,k=K,"floor")
  graphen.bloat.edge <- bloat.edgestream(Gs.edgelists, steps_allowed) #verschiedene ks
  graphen.edges <- do.call(rbind,graphen.bloat.edge)
  KA_neu <- t_closeness(graphen.edges,numV,length(zeiten))
  
  graphen.bloat.edge <- bloat.edgestream(Gs.edgelists, rep(1,length(zeiten))) # k=1 nach K&A
  graphen.edges <- do.call(rbind,graphen.bloat.edge)
  KA_alt <- t_closeness(graphen.edges,numV,length(zeiten))
  
  
  
  Gs.edgelists.TRUTH <- lapply(1:numT, FUN=function(x){
    E1 =get.edgelist(Gs[[x]])
    ### Hier m?ssen die Kanten jetzt noch verdoppelt werden, 
    # sodass neben A-B auch B-A auftaucht!
    #E2 = rbind(E1, E1[ ,c(2,1)])
  })
  
  graphen.bloat.edge.TRUTH <- bloat.edgestream(Gs.edgelists.TRUTH, rep(1,numT))
  graphen.edges.TRUTH <- do.call(rbind,graphen.bloat.edge.TRUTH)
  KA_TRUTH <- t_closeness(graphen.edges.TRUTH, numV, numT)
  
  
  
  out <- list()
  out[[1]] <- KA_TRUTH
  out[[2]] <- KA_alt
  out[[3]] <- KA_neu
  out[[4]] <- zeiten
  out[[5]] <- summaryDegree
  out[[6]] <- sdDegreeTotal
  out[[7]] <- nEdgesTotal
  out[[8]] <- closenessStatic
  out[[9]] <- nEdgesStar
  out[[10]] <- StartSternTimepoint
  out[[11]] <- ClosenessStar
  out[[12]] <- Stern
  out[[13]] <- paste("Type of Simulation: ", TypeOfSimulation,"\n",
                     "Actual number of Simulation: ", x, "\n",
                     "Numberof Vertices: ", numV, "\n",
                     "Number of Timepoints: ", "Levels of Star * ", FaktorTimepoints, "\n",
                     "Actual number of Timepoints: ", length(Gs), "\n",
                     "p or m for ER-Modell: ", "Number of Star egdes * ", AnteilKanten , "\n",
                     "Number of Steps to 1st Snapshot: ", K, "\n",
                     "Actual Number of Levels of Star", levelStern, "\n",
                     "Actual Number of Snapshots: ", length(zeiten)
  )
  
  names(out) <- c("KA_TRUTH", "KA_alt", "KA_neu", "zeiten", 
                  "summaryDegree", "sdDegreeTotal", "nEdgesTotal", 
                  "closenessStatic", "nEdgesStar", "StartSternTimepoint",
                  "ClosenessStar", "Stern", "Infos")
  out
})




save(TmpCent_Simu_StarGrowEdgeLasting3_Known_1000_2levelStar_0.25m_500_DIRECTED, 
     file="/home/hanke/Ergebnisse/Tmp-Cent/Simu/TmpCent_Simu_StarGrowEdgeLasting3_Known_1000_2levelStar_0.25m_500_DIRECTED.RData")


stopCluster(cl)
mpi.quit()
