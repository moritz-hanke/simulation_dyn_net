library( parallel)
library( snow, lib.loc = "~/Rpackages")
library( Rmpi, lib.loc="~/Rpackages")

library( igraph, lib.loc = "~/Rpackages")


source("/home/hanke/Programme/Tmp-Cent/Funktionen/Funktion_t_closeness.R")
source("/home/hanke/Programme/Tmp-Cent/Funktionen/Funktion_k-Schritte-pro-Intervall.R")
source("/home/hanke/Programme/Tmp-Cent/Funktionen/Funktion_bloat.edgestream.R")
source("/home/hanke/Programme/Tmp-Cent/Funktionen/Sterngraphen.R")



### Create cluster and export required objects
mc<-250#Optional: number of cores can be determined automatically
cl <- makeCluster(mc, type="MPI")

TypeOfSimulation <- "Shrinking Star (begining at inner level) with Noise;
                    Known: number of timesteps till first snapshot is known"
NSimu <- 500

clusterExport(cl, c("bloat.edgestream","intervalle.k","t_closeness", "erdos.renyi.game", 
                    "graph.union", "get.edgelist", "NSimu", "E", "degree", "centr_clo",
                    "sterngraph_edgelist", "graph_from_edgelist", "NSimu", "TypeOfSimulation"))

TmpCent_Simu_StarShrinkInnerLevel_Known_1000_2levelStar_0.25m_500 <- parLapplyLB(cl=cl, 1:NSimu, fun=function(x){
  
  
  ### Art der Simulation ###
  #~~~~~~~~~~~~~~~~~~~~~~~~#
  # Wachsender Sterngraph 
  
  # Anzahl Schritte
  K=1
  # Anzahl Knoten
  numV <-1000
  # Wie hoch soll der Anteil an Kanten im Noisegraphen im Verh?ltnis zum
  # Sterngraphen sein
  AnteilKanten <-0.25
  # Um welchen Faktor soll die Anzahl an Zeitpuntken numT die Anzahl an
  # Levels des Sterngraphen ?bersteigen
  FaktorTimepoints <- 2
  
  start.seed <-x+43401 
  set.seed(start.seed)
  
  # Simulation des Sterns
  Stern <- sterngraph_edgelist(20, neighbours_start = c(10:20), 
                               neighbours_rest = c(0:3), one_list=FALSE,
                               cut_node=1000)
  
  # Wieviele Level, d.h. wie viele Generationen werden so erzeugt
  levelStern <- length(Stern)
  numT <- levelStern*FaktorTimepoints
  start.seed <-x+43401 
  set.seed(start.seed)
  
  # Wie viele Snapshots
  nSnapshots <- sample(floor(numT/4):(numT/2),1)
  
  # Anzahl an Kanten im Noisegraphen
  m <- floor((max(Stern[[length(Stern)]][,2])-1)*AnteilKanten)
  
  
  Gs.sternshrink <- vector(mode="list", length=numT)
  nEdgesTotal <- vector(mode="list", length=numT)
  summaryDegree <- vector(mode="list", length=numT)
  sdDegreeTotal <- vector(mode="list", length=numT)
  closenessStatic <- vector(mode="list", length=numT)
  
  start.seed <-x+43401
  set.seed(start.seed)
  Gs.sternshrink[[1]] <- graph.union(erdos.renyi.game(numV, m, "gnm"),
                                   graph_from_edgelist(do.call(rbind,Stern), FALSE)
  )
  ### um zu verhindern, dass der Stargraph leer ist
  while(length(E(Gs.sternshrink[[1]]))<1){
    start.seed <- start.seed+1
    set.seed(start.seed)
    Gs.sternshrink[[1]] <- graph.union(erdos.renyi.game(numV, m, "gnm"),
                                     graph_from_edgelist(do.call(rbind,Stern))
    )
    
    
  }
  
  
  StartSternTimepoint <- sample(1:(numT-levelStern+1),1)
  
  
  for(i in 2:numT){
    set.seed(x+i*i+i*736000)
    Gs.sternshrink[[i]] <- graph.union(erdos.renyi.game(numV, m, "gnm"),
                                     graph_from_edgelist(do.call(rbind,Stern), FALSE)
    )
  }
  
  nEdgesStar <- lapply(seq_along(Stern), FUN=function(f){
    max(Stern[[f]][,2])-1
  })
  
  SternUnion <- NULL
  sternindex <- 0
  for(i in StartSternTimepoint:numT){
    sternindex <- sternindex+1
    # Da der Sterngraph bleibend ist, muss gepr?ft werden, ob bereits der letzte
    # Level des Sterngraphs mit den Zufallsgraphen zusammengef?gt worden ist
    # wenn ja, wird in den ggf. noch weiteren Zeitpunkten der immer selber
    # komplette Sterngraph eingef?gt
    if(sternindex>length(Stern)){
      Gs.sternshrink[[i]] <- Gs.sternshrink[[i]] - graph_from_edgelist(do.call(rbind, Stern),
                                                                   FALSE)
    }else{
      Gs.sternshrink[[i]] <- Gs.sternshrink[[i]]- 
        graph_from_edgelist(do.call(rbind, Stern[1:sternindex]), FALSE)              
    }
    
  }
  
  
  nEdgesTotal[[1]] <- length(E(Gs.sternshrink[[1]]))
  summaryDegree[[1]] <- summary(degree(Gs.sternshrink[[1]]))
  sdDegreeTotal[[1]] <- sd(degree(Gs.sternshrink[[1]]))
  closenessStatic[[1]] <- centr_clo(Gs.sternshrink[[1]])
  for(i in 2:length(Gs.sternshrink)){
    nEdgesTotal[[i]] <- length(E(Gs.sternshrink[[i]]))
    summaryDegree[[i]] <- summary(degree(Gs.sternshrink[[i]]))
    sdDegreeTotal[[i]] <- sd(degree(Gs.sternshrink[[i]]))
    closenessStatic[[i]] <- centr_clo(Gs.sternshrink[[i]])
  }
  
  ClosenessStar <- 
    centr_clo(graph.union(lapply(Stern, graph_from_edgelist, directed=FALSE)))
  
  
  Gs <- Gs.sternshrink
  #~~~~~~~~~~~~~~~~~~~~~~~~#
  
  
  set.seed(x)
  zeiten <- sort(sample(1:numT, nSnapshots, replace=FALSE))
  ### da es passieren kann, dass alle Snapshots au?erhalb des Intervalls,
  # in dem der Stern exisitiert, liegen, m?ssen so lange neue Zeiten
  # gezogen werden, bis mindestens ein Snapshot einmal den Stern beinhaltet
  while(!any(zeiten  %in% StartSternTimepoint:(StartSternTimepoint+levelStern-1))){
    x <- x+12749
    set.seed(x)
    zeiten <- sort(sample(1:numT, nSnapshots, replace=FALSE))
  }
  
  Gs.edgelists <- lapply(zeiten, FUN=function(x){
    E1 =get.edgelist(Gs[[x]])
    ### Hier m?ssen die Kanten jetzt noch verdoppelt werden, 
    # sodass neben A-B auch B-A auftaucht!
    E2 = rbind(E1, E1[ ,c(2,1)])
  })
  
  first <- zeiten[1]
  # K wird angepasst
  if(length(zeiten)>1){
    K <- min(diff(sort(zeiten)))
  }else{
    K <- zeiten[1]
  }
  
  steps_allowed <- intervalle.k(zeiten, first ,k=K,"floor")
  graphen.bloat.edge <- bloat.edgestream(Gs.edgelists, steps_allowed) #verschiedene ks
  graphen.edges <- do.call(rbind,graphen.bloat.edge)
  KA_neu <- t_closeness(graphen.edges,numV,length(zeiten))
  
  graphen.bloat.edge <- bloat.edgestream(Gs.edgelists, rep(1,length(zeiten))) # k=1 nach K&A
  graphen.edges <- do.call(rbind,graphen.bloat.edge)
  KA_alt <- t_closeness(graphen.edges,numV,length(zeiten))
  
  
  
  Gs.edgelists.TRUTH <- lapply(1:numT, FUN=function(x){
    E1 =get.edgelist(Gs[[x]])
    ### Hier m?ssen die Kanten jetzt noch verdoppelt werden, 
    # sodass neben A-B auch B-A auftaucht!
    E2 = rbind(E1, E1[ ,c(2,1)])
  })
  
  graphen.bloat.edge.TRUTH <- bloat.edgestream(Gs.edgelists.TRUTH, rep(1,numT))
  graphen.edges.TRUTH <- do.call(rbind,graphen.bloat.edge.TRUTH)
  KA_TRUTH <- t_closeness(graphen.edges.TRUTH, numV, numT)
  
  
  
  out <- list()
  out[[1]] <- KA_TRUTH
  out[[2]] <- KA_alt
  out[[3]] <- KA_neu
  out[[4]] <- zeiten
  out[[5]] <- summaryDegree
  out[[6]] <- sdDegreeTotal
  out[[7]] <- nEdgesTotal
  out[[8]] <- closenessStatic
  out[[9]] <- nEdgesStar
  out[[10]] <- StartSternTimepoint
  out[[11]] <- ClosenessStar
  out[[12]] <- Stern
  out[[13]] <- paste("Type of Simulation: ", TypeOfSimulation,"\n",
                     "Actual number of Simulation: ", x, "\n",
                     "Numberof Vertices: ", numV, "\n",
                     "Number of Timepoints: ", "Levels of Star * ", FaktorTimepoints, "\n",
                     "Actual number of Timepoints: ", length(Gs), "\n",
                     "p or m for ER-Modell: ", "Number of Star egdes * ", AnteilKanten , "\n",
                     "Number of Steps to 1st Snapshot: ", K, "\n",
                     "Actual Number of Levels of Star", levelStern, "\n",
                     "Actual Number of Snapshots: ", length(zeiten)
  )
  
  names(out) <- c("KA_TRUTH", "KA_alt", "KA_neu", "zeiten", 
                  "summaryDegree", "sdDegreeTotal", "nEdgesTotal", 
                  "closenessStatic", "nEdgesStar", "StartSternTimepoint",
                  "ClosenessStar", "Stern", "Infos")
  out
})

save(TmpCent_Simu_StarShrinkInnerLevel_Known_1000_2levelStar_0.25m_500, 
     file="/home/hanke/Ergebnisse/Tmp-Cent/Simu/TmpCent_Simu_StarShrinkInnerLevel_Known_1000_2levelStar_0.25m_500.RData")


stopCluster(cl)
mpi.quit()
