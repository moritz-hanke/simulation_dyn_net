library(igraph)
###############################################################
# Funktion um einen Graphen zu erzeugen, der einen Satrtknoten
# hat von dem aus sich ein Stern aufspannt, an dessen Enden sich
# neue Sterne aufspannen usw.; die Gr��e der Sterne wird mit
# der sample()-Funktion zuf�llig bestimmt; dazu wird ein Vektor
# angegeben, aus dem gezogen wird
# 
# OUTPUT: Liste mit Kantenlisten pro Sternebene oder nur eine
#         Kantenliste (siehe one_list)
#
# INPUT:
# width:  wie viele Ebenen an Sternen soll es geben bzw. in wie
#         vielen Kantenschritten ist man vom Startknoten an den
#         �u�ersten Knoten
# neighbours_start:   Vektor; mit wie vielen Kanten soll 
#                     gestartet werden
# neigbours_rest:   Vektor; wie viele Kanten haben die anderen 
#                   Sterne
# one_list:   logical; sollen die einzelnen Edgelisten in als
#             Elemente einer Liste gespeichert werden pro Stern-
#             ebene oder als einfache Kantenliste
# cut_node:   wie viele Knoten sollen mit der Methode maximal
#             enstehen?

sterngraph_edgelist <- function(width=NULL,
                             neighbours_start=NULL,
                             neighbours_rest=NULL,
                             one_list=FALSE,
                             cut_node=NULL){
  if(length(neighbours_start)<2){
    stop("neighbours_start has to be a vector")
  }
  if(length(neighbours_rest)<2){
    stop("neighbours has to be a vector")
  }
  if(width < 2){
    stop("width has to be at least 2; for a normal star use 
         'star()' from the igrapg package")
  }

  sternliste <- list()
  
  a1 <- sample(neighbours_start,1) # Anzahl Kanten am Startknoten
  b1 <- 1:a1+1 # Entsprechende Knoten erstellen
  sternliste[[1]] <- cbind(rep(1,a1), b1) # Kantenliste
  colnames(sternliste[[1]]) <- NULL
  max.knoten <- max(b1) # wird ben�tigt um zu wissen, welchen Namen
                        # die n�chsten Knoten bekommen m�ssen 
  sternliste.tmp <- NULL
  
  while(length(sternliste)<2){  # while ist n�tig, falls zuf�llig nur
                                # 0 gezogen werden, dann gibt es nur die
                                # weiter oben erzeugte Liste, also einen
                                # Stern
    for(i in b1){
      a2 <- sample(neighbours_rest,1)
      if(a2 >0){
        b2 <- 1:a2+max.knoten
        sternliste.tmp2 <- cbind(rep(i,a2), b2)
        colnames(sternliste.tmp2) <- NULL
        sternliste.tmp <- rbind(sternliste.tmp,sternliste.tmp2)
        max.knoten <- max(b2)
      }
      
    }
    sternliste[[2]] <- sternliste.tmp
  }
  
  
  if(width>2){
    for(p in 3:width){
      
      sternliste.tmp <- NULL
      for(l in sternliste[[p-1]][,2]){
        a2 <- sample(neighbours_rest,1)
        
        if(a2 > 0){ # Benennung der Knoten die am Ende sind; 
                    # wird sp�ter angepasst, hier
          sternliste.tmp[[l]] <- cbind(rep(l, a2), paste(l,"_", 1:a2))
        }
      }
      
      
      sternliste.tmp <- do.call(rbind, sternliste.tmp)
      # richtige Benennung der Endknoten; max wird vom letzten Knoten
      # der vorrigen "Runde" an erzeugten Knoten genommen
      sternliste.tmp[,2] <- seq_along(sternliste.tmp[,2])+max(sternliste[[p-1]][,2])
      sternliste.tmp <- matrix(as.numeric(sternliste.tmp), ncol=2, byrow=F)
      
      # wenn durch das Sternverfahren mehr Knoten erzeugt werden, als
      # gew�nscht, wird die Funktion hier abgebrochen, sodass die Liste
      # mit den zuviel Knoten wegf�llt; d.h. es werden blo� <=cut_node 
      # Knoten erzeugt 
      if(max(sternliste.tmp[,2])> cut_node & !is.null(cut_node)){
        break
      }
      
      sternliste[[p]] <- sternliste.tmp
    
    }
    
  }

  # sollen die einzelnen Sternebenen in als Listenelemente vorliegen
  # oder alles in einer Edgeliste:
  if(one_list==TRUE){
    sternliste <- do.call(rbind,sternliste)
  }
  sternliste
}


