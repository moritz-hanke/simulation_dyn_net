library( parallel)
library( snow, lib.loc = "~/Rpackages")
library( Rmpi, lib.loc = "~/Rpackages")

library( igraph, lib.loc = "~/Rpackages")


source("/home/hanke/Programme/Tmp-Cent/Funktionen/Funktion_t_closeness.R")
source("/home/hanke/Programme/Tmp-Cent/Funktionen/Funktion_k-Schritte-pro-Intervall.R")
source("/home/hanke/Programme/Tmp-Cent/Funktionen/Funktion_bloat.edgestream.R")
source("/home/hanke/Programme/Tmp-Cent/Funktionen/GroupInfection.R")
source("/home/hanke/Programme/Tmp-Cent/Funktionen/JumpingGroupInfection.R")




### Create cluster and export required objects
mc<-250#Optional: number of cores can be determined automatically
cl <- makeCluster(mc, type="MPI")

TypeOfSimulation <- "JumpingGroupInfection; No Noise; Only Snapshots from evolving,
                    Time till first Snapshot known; Edgelasting =1; DIRECTED"
NSimu <- 500

clusterExport(cl, c("bloat.edgestream","intervalle.k","t_closeness", "erdos.renyi.game", 
                    "graph.union", "get.edgelist", "NSimu", "E", "degree", "centr_clo",
                    "GroupInfection", "graph_from_edgelist", "NSimu", "TypeOfSimulation",
                    "JumpingGroupInfection", "centr_clo", "centr_eigen", "centr_betw",
                    "make_empty_graph", "graph.union"))

TmpCent_Simu_JGI_Edgelasting1_5_40_0.025_4_Close_KNOWN_NONOISE_DIRECTED <- parLapplyLB(cl=cl, 1:NSimu, fun=function(x){
  
  
  ### Art der Simulation ###
  #~~~~~~~~~~~~~~~~~~~~~~~~#
  #Jumping Group Infection ohne Rauschen; Edgelasting; DIRECTED
  
  # Anzahl Schritte
  K=1
  # Anzahl Knoten
  numV <-200
  
  #Edgelasting
  Edgelasting <- 1
  
  JGI <- JumpingGroupInfection(nGroups = 5, 
                               vertex.ids = 1:40, 
                               start.vertex = 1, 
                               p_start = 0.025, 
                               evolving_steps = 4, 
                               less_important = "closeness", 
                               x=x)
  
  
  ### Kanten sollen bleiben!!!
  Gs.JGI <- lapply(seq_along(JGI), FUN=function(y){
    
    if((y-Edgelasting) <= 0){
      out <- do.call(rbind, JGI[c((y-Edgelasting):y)[c((y-Edgelasting):y)>0]])
    }else {
      out <- do.call(rbind, JGI[(1+y-Edgelasting):y])
    }
    out
  })
  
  # Wieviele Level, d.h. wie viele Generationen werden so erzeugt
  
  numT <- length(JGI)
  start.seed <-x+54002 
  set.seed(start.seed)
  
  # Wie viele Snapshots
  nSnapshots <- sample(floor(numT/4):(numT/2),1)
  
  
  set.seed(x)
  zeiten <- sort(sample(1:numT, nSnapshots, replace=FALSE))
  
  ### weil DIRECTED
  Gs.edgelists <- lapply(zeiten, FUN=function(x){
    #out <- rbind(Gs.JGI[[x]], Gs.JGI[[x]][,c(2,1)])
    out <- Gs.JGI[[x]]
    out
  })
  
  first <- zeiten[1]
  # K wird angepasst
  if(length(zeiten)>1){
    K <- min(diff(sort(zeiten)))
  }else{
    K <- zeiten[1]
  }
  
  steps_allowed <- intervalle.k(zeiten, first ,k=K,"floor")
  graphen.bloat.edge <- bloat.edgestream(Gs.edgelists, steps_allowed) #verschiedene ks
  graphen.edges <- do.call(rbind,graphen.bloat.edge)
  KA_neu <- t_closeness(graphen.edges,numV,length(zeiten))
  
  graphen.bloat.edge <- bloat.edgestream(Gs.edgelists, rep(1,length(zeiten))) # k=1 nach K&A
  graphen.edges <- do.call(rbind,graphen.bloat.edge)
  KA_alt <- t_closeness(graphen.edges,numV,length(zeiten))
  
  
  
  Gs.edgelists.TRUTH <- lapply(1:numT, FUN=function(x){
    #out <- rbind(Gs.JGI[[x]], Gs.JGI[[x]][,c(2,1)])
    out <- Gs.JGI[[x]]
    out
  })
  
  graphen.bloat.edge.TRUTH <- bloat.edgestream(Gs.edgelists.TRUTH, rep(1,numT))
  graphen.edges.TRUTH <- do.call(rbind,graphen.bloat.edge.TRUTH)
  KA_TRUTH <- t_closeness(graphen.edges.TRUTH, numV, numT)
  
  
  
  out <- list()
  out[[1]] <- KA_TRUTH
  out[[2]] <- KA_alt
  out[[3]] <- KA_neu
  out[[4]] <- zeiten
  out[[5]] <- JGI
  out[[6]] <- TypeOfSimulation
  
  out
})




save(TmpCent_Simu_JGI_Edgelasting1_5_40_0.025_4_Close_KNOWN_NONOISE_DIRECTED, 
     file="/home/hanke/Ergebnisse/Tmp-Cent/Simu/TmpCent_Simu_JGI_Edgelasting1_5_40_0.025_4_Close_KNOWN_NONOISE_DIRECTED.RData")


stopCluster(cl)
mpi.quit()
