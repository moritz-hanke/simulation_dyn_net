library( parallel)
library( snow, lib.loc = "~/Rpackages")
library( Rmpi, lib.loc="~/Rpackages")

library( igraph, lib.loc = "~/Rpackages")


source("/home/hanke/Programme/Tmp-Cent/Funktionen/Funktion_t_closeness.R")
source("/home/hanke/Programme/Tmp-Cent/Funktionen/Funktion_k-Schritte-pro-Intervall.R")
source("/home/hanke/Programme/Tmp-Cent/Funktionen/Funktion_bloat.edgestream.R")
source("/home/hanke/Programme/Tmp-Cent/Funktionen/Sterngraphen.R")



### Create cluster and export required objects
mc<-250#Optional: number of cores can be determined automatically
cl <- makeCluster(mc, type="MPI")

TypeOfSimulation <- "Growing, constant Star; NONOISE; 
                    Known: number of timesteps till first snapshot is known"
NSimu <- 500

clusterExport(cl, c("bloat.edgestream","intervalle.k","t_closeness", "erdos.renyi.game", 
                    "graph.union", "get.edgelist", "NSimu", "E", "degree", "centr_clo",
                    "sterngraph_edgelist", "graph_from_edgelist", "NSimu", "TypeOfSimulation",
                    "make_empty_graph"))

TmpCent_Simu_StarGrow_Known_1000_2levelStar_0.25m_500_NONOISE <- parLapplyLB(cl=cl, 1:NSimu, fun=function(x){
  
  
  ### Art der Simulation ###
  #~~~~~~~~~~~~~~~~~~~~~~~~#
  # Wachsender Sterngraph 
  
  # Anzahl Schritte
  K=1
  # Anzahl Knoten
  numV <-1000
  # Um welchen Faktor soll die Anzahl an Zeitpuntken numT die Anzahl an
  # Levels des Sterngraphen ?bersteigen
  FaktorTimepoints <- 2
  
  start.seed <-x+27901 
  set.seed(start.seed)
  
  # Simulation des Sterns
  Stern <- sterngraph_edgelist(20, neighbours_start = c(10:20), 
                               neighbours_rest = c(0:3), one_list=FALSE,
                               cut_node=1000)
  
  # Wieviele Level, d.h. wie viele Generationen werden so erzeugt
  levelStern <- length(Stern)
  numT <- levelStern*FaktorTimepoints
  start.seed <-x+27902 
  set.seed(start.seed)
  
  # Wie viele Snapshots
  nSnapshots <- sample(floor(numT/4):(numT/2),1)
  
  
  
  Gs.sterngrow <- vector(mode="list", length=numT)
  nEdgesTotal <- vector(mode="list", length=numT)
  summaryDegree <- vector(mode="list", length=numT)
  sdDegreeTotal <- vector(mode="list", length=numT)
  closenessStatic <- vector(mode="list", length=numT)
  
  start.seed <-x+27903
  set.seed(start.seed)
  Gs.sterngrow[[1]] <- make_empty_graph(numV, directed=FALSE)
  
  
  StartSternTimepoint <- sample(1:(numT-levelStern+1),1)
  
  
  for(i in 2:numT){
    set.seed(x+i*i+i*279000)
    Gs.sterngrow[[i]] <- make_empty_graph(numV, directed=FALSE)
  }
  
  nEdgesStar <- lapply(seq_along(Stern), FUN=function(f){
    max(Stern[[f]][,2])-1
  })
  
  SternUnion <- NULL
  sternindex <- 0
  for(i in StartSternTimepoint:numT){
    sternindex <- sternindex+1
    # Da der Sterngraph bleibend ist, muss gepr?ft werden, ob bereits der letzte
    # Level des Sterngraphs mit den Zufallsgraphen zusammengef?gt worden ist
    # wenn ja, wird in den ggf. noch weiteren Zeitpunkten der immer selber
    # komplette Sterngraph eingef?gt
    if(sternindex>levelStern){
      SternUnion <- graph.union(SternUnion, graph_from_edgelist(Stern[[levelStern]],FALSE))
      Gs.sterngrow[[i]] <- graph.union(Gs.sterngrow[[i]],SternUnion)
    }else{
      SternUnion <- graph.union(SternUnion, graph_from_edgelist(Stern[[sternindex]],FALSE))
      Gs.sterngrow[[i]] <- graph.union(Gs.sterngrow[[i]],SternUnion)
    }
    
  }
  
  
  nEdgesTotal[[1]] <- length(E(Gs.sterngrow[[1]]))
  summaryDegree[[1]] <- summary(degree(Gs.sterngrow[[1]]))
  sdDegreeTotal[[1]] <- sd(degree(Gs.sterngrow[[1]]))
  closenessStatic[[1]] <- centr_clo(Gs.sterngrow[[1]])
  for(i in 2:length(Gs.sterngrow)){
    nEdgesTotal[[i]] <- length(E(Gs.sterngrow[[i]]))
    summaryDegree[[i]] <- summary(degree(Gs.sterngrow[[i]]))
    sdDegreeTotal[[i]] <- sd(degree(Gs.sterngrow[[i]]))
    closenessStatic[[i]] <- centr_clo(Gs.sterngrow[[i]])
  }
  
  ClosenessStar <- 
    centr_clo(graph.union(lapply(Stern, graph_from_edgelist, directed=FALSE)))
  
  
  Gs <- Gs.sterngrow
  #~~~~~~~~~~~~~~~~~~~~~~~~#
  
  
  set.seed(x)
  zeiten <- sort(sample(1:numT, nSnapshots, replace=FALSE))
  ### da es passieren kann, dass alle Snapshots au?erhalb des Intervalls,
  # in dem der Stern exisitiert, liegen, m?ssen so lange neue Zeiten
  # gezogen werden, bis mindestens ein Snapshot einmal den Stern beinhaltet
  while(!any(zeiten  %in% StartSternTimepoint:(StartSternTimepoint+levelStern-1))){
    x <- x+12749
    set.seed(x)
    zeiten <- sort(sample(1:numT, nSnapshots, replace=FALSE))
  }
  
  Gs.edgelists <- lapply(zeiten, FUN=function(x){
    E1 =get.edgelist(Gs[[x]])
    ### Hier m?ssen die Kanten jetzt noch verdoppelt werden, 
    # sodass neben A-B auch B-A auftaucht!
    E2 = rbind(E1, E1[ ,c(2,1)])
  })
  
  first <- zeiten[1]
  # K wird angepasst
  if(length(zeiten)>1){
    K <- min(diff(sort(zeiten)))
  }else{
    K <- zeiten[1]
  }
  
  steps_allowed <- intervalle.k(zeiten, first ,k=K,"floor")
  graphen.bloat.edge <- bloat.edgestream(Gs.edgelists, steps_allowed) #verschiedene ks
  graphen.edges <- do.call(rbind,graphen.bloat.edge)
  ### dass sum(sapply(...)) mus eingef?gt werden, weil es Graphen ohne Kanten
    # geben kann, die aber als Snapshot betrachtet wurden und folglich in
    # graphen.bloat.edge ein NULL Element sind. Dad?rch ist dann lentgth(zeiten)
    # gr??er als die Anzahl an auszuwertenden Graphen und t_closeness wirft einen
    # Fehler aus 
  KA_neu <- t_closeness(graphen.edges,numV,length(zeiten)-sum(sapply(graphen.bloat.edge, is.null)))
  
  graphen.bloat.edge <- bloat.edgestream(Gs.edgelists, rep(1,length(zeiten))) # k=1 nach K&A
  graphen.edges <- do.call(rbind,graphen.bloat.edge)
  KA_alt <- t_closeness(graphen.edges,numV,length(zeiten)-sum(sapply(graphen.bloat.edge, is.null)))
  
  
  
  
  Gs.edgelists.TRUTH <- lapply(1:numT, FUN=function(x){
    E1 =get.edgelist(Gs[[x]])
    ### Hier m?ssen die Kanten jetzt noch verdoppelt werden, 
    # sodass neben A-B auch B-A auftaucht!
    E2 = rbind(E1, E1[ ,c(2,1)])
  })
  
  graphen.bloat.edge.TRUTH <- bloat.edgestream(Gs.edgelists.TRUTH, rep(1,numT))
  graphen.edges.TRUTH <- do.call(rbind,graphen.bloat.edge.TRUTH)
  KA_TRUTH <- t_closeness(graphen.edges.TRUTH, numV, 
                          numT-sum(sapply(graphen.bloat.edge.TRUTH, is.null)))
  
  
  
  out <- list()
  out[[1]] <- KA_TRUTH
  out[[2]] <- KA_alt
  out[[3]] <- KA_neu
  out[[4]] <- zeiten
  out[[5]] <- summaryDegree
  out[[6]] <- sdDegreeTotal
  out[[7]] <- nEdgesTotal
  out[[8]] <- closenessStatic
  out[[9]] <- nEdgesStar
  out[[10]] <- StartSternTimepoint
  out[[11]] <- ClosenessStar
  out[[12]] <- Stern
  out[[13]] <- paste("Type of Simulation: ", TypeOfSimulation,"\n",
                     "Actual number of Simulation: ", x, "\n",
                     "Numberof Vertices: ", numV, "\n",
                     "Number of Timepoints: ", "Levels of Star * ", FaktorTimepoints, "\n",
                     "Actual number of Timepoints: ", length(Gs), "\n",
			"NONOISE", "\n",
                     "Number of Steps to 1st Snapshot: ", K, "\n",
                     "Actual Number of Levels of Star", levelStern, "\n",
                     "Actual Number of Snapshots: ", length(zeiten)
  )
  
  names(out) <- c("KA_TRUTH", "KA_alt", "KA_neu", "zeiten", 
                  "summaryDegree", "sdDegreeTotal", "nEdgesTotal", 
                  "closenessStatic", "nEdgesStar", "StartSternTimepoint",
                  "ClosenessStar", "Stern", "Infos")
  out
})

save(TmpCent_Simu_StarGrow_Known_1000_2levelStar_0.25m_500_NONOISE, 
     file="/home/hanke/Ergebnisse/Tmp-Cent/Simu/TmpCent_Simu_StarGrow_Known_1000_2levelStar_0.25m_500_NONOISE.RData")


stopCluster(cl)
mpi.quit()
