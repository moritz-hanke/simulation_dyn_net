library( parallel)
library( snow, lib.loc = "~/Rpackages")
library( Rmpi, lib.loc="~/Rpackages")

library( igraph, lib.loc = "~/Rpackages")


source("/home/hanke/Programme/Tmp-Cent/Funktionen/Funktion_t_closeness.R")
source("/home/hanke/Programme/Tmp-Cent/Funktionen/Funktion_k-Schritte-pro-Intervall.R")
source("/home/hanke/Programme/Tmp-Cent/Funktionen/Funktion_bloat.edgestream.R")

### Create cluster and export required objects
mc<-125#Optional: number of cores can be determined automatically
cl <- makeCluster(mc, type="MPI")


NSimu <- 500
TypeOfSimulation <- "Pure Growing Random Graphs"


clusterExport(cl, c("bloat.edgestream","intervalle.k","t_closeness", "erdos.renyi.game", 
                    "graph.union", "get.edgelist", "NSimu", "E", "degree", "centr_clo",
                    "NSimu", "TypeOfSimulation"))

TmpCent_Simu_PureGrow_1_1000_50_0.0005_500 <- parLapplyLB(cl=cl, 1:NSimu, fun=function(x){
   
  
  ### Art der Simulation ###
  #~~~~~~~~~~~~~~~~~~~~~~~~#
  # Zuf�llige Graphen, die nur wachsen
  
  K=1
  numV <-1000
  numT <- 50
  start.seed <-x+10001 
  set.seed(start.seed)
  nSnapshots <- sample(5:10,1)
  p <- 0.0005
  
  Gs.puregrow <- vector(mode="list", length=numT)
  nEdges <- vector(mode="list", length=numT)
  summaryDegree <- vector(mode="list", length=numT)
  sdDegree <- vector(mode="list", length=numT)
  closenessStatic <- vector(mode="list", length=numT)
  
  start.seed <-x+10001 
  set.seed(start.seed)
  Gs.puregrow[[1]] <- erdos.renyi.game(numV, p)
  ### um zu verhindern, dass der Stargraph leer ist
  while(length(E(Gs.puregrow[[1]]))<1){
    start.seed <- start.seed+1
    set.seed(start.seed)
    Gs.puregrow[[1]] <- erdos.renyi.game(numV, p)
    
  }
  
  nEdges[[1]] <- length(E(Gs.puregrow[[1]]))
  summaryDegree[[1]] <- summary(degree(Gs.puregrow[[1]]))
  sdDegree[[1]] <- sd(degree(Gs.puregrow[[1]]))
  closenessStatic[[1]] <- centr_clo(Gs.puregrow[[1]])
  
  Gunion <- NULL
  
  
  for(i in 2:numT){
    set.seed(x+i*i+i*100000)
    Gunion <- graph.union(Gs.puregrow[[i-1]], erdos.renyi.game(numV, p/i))
    Gs.puregrow[[i]] <- Gunion
    nEdges[[i]] <- length(E(Gs.puregrow[[i]]))
    summaryDegree[[i]] <- summary(degree(Gs.puregrow[[i]]))
    sdDegree[[i]] <- sd(degree(Gs.puregrow[[i]]))
    closenessStatic[[i]] <- centr_clo(Gs.puregrow[[i]])
  }
  
  
  Gs <- Gs.puregrow
  #~~~~~~~~~~~~~~~~~~~~~~~~#
  
  
  set.seed(x)
  zeiten <- sort(sample(1:numT, nSnapshots, replace=FALSE))
  
  Gs.edgelists <- lapply(zeiten, FUN=function(x){
    E1 =get.edgelist(Gs[[x]])
    ### Hier m�ssen die Kanten jetzt noch verdoppelt werden, 
      # sodass neben A-B auch B-A auftaucht!
    E2 = rbind(E1, E1[ ,c(2,1)])
  })
  
  # K wird angepasst
  if(length(zeiten)>1){
    K <- min(diff(sort(zeiten)))
  }else{
    K <- zeiten[1]
  }
  
  steps_allowed <- intervalle.k(zeiten, 1 ,k=K,"floor")
  graphen.bloat.edge <- bloat.edgestream(Gs.edgelists, steps_allowed) #verschiedene ks
  graphen.edges <- do.call(rbind,graphen.bloat.edge)
  KA_neu <- t_closeness(graphen.edges,numV,length(zeiten))
  
  graphen.bloat.edge <- bloat.edgestream(Gs.edgelists, rep(1,length(zeiten))) # k=1 nach K&A
  graphen.edges <- do.call(rbind,graphen.bloat.edge)
  KA_original <- t_closeness(graphen.edges,numV,length(zeiten))
  
  
  
  Gs.edgelists.TRUTH <- lapply(1:numT, FUN=function(x){
    E1 =get.edgelist(Gs[[x]])
    ### Hier m�ssen die Kanten jetzt noch verdoppelt werden, 
    # sodass neben A-B auch B-A auftaucht!
    E2 = rbind(E1, E1[ ,c(2,1)])
  })
  
  graphen.bloat.edge.TRUTH <- bloat.edgestream(Gs.edgelists.TRUTH, rep(1,numT))
  graphen.edges.TRUTH <- do.call(rbind,graphen.bloat.edge.TRUTH)
  KA_TRUTH <- t_closeness(graphen.edges.TRUTH, numV, numT)
  
  
  
  out <- list()
  out[[1]] <- KA_TRUTH
  out[[2]] <- KA_original
  out[[3]] <- KA_neu
  out[[4]] <- zeiten
  out[[5]] <- summaryDegree
  out[[6]] <- sdDegree
  out[[7]] <- nEdges
  out[[8]] <- closenessStatic
  out[[9]] <- paste("Type of Simulation: ", TypeOfSimulation,"\n",
                    "Actual number of Simulation: ", x, "\n",
                    "Numberof Vertices: ", numV, "\n",
                    "Number of Timepoints: ", numT, "\n",
                    "Actual number of Timepoints: ", length(Gs), "\n",
                    "p or m for ER-Modell: ", p, "\n",
                    "Number of Steps to 1st Snapshot: ", K, "\n",
                    "Actual Number of Snapshots: ", length(zeiten)
  )
                    
  names(out) <- c("KA_TRUTh", "KA_original", "KA_neu", "zeiten", 
                  "summaryDegree", "sdDegree", "nEdges", 
                  "closenessStatic", "Infos")
  out
})

save(TmpCent_Simu_PureGrow_1_1000_50_0.0005_500, 
     file="/home/hanke/Ergebnisse/Tmp-Cent/Simu/TmpCent_Simu_PureGrow_1_1000_50_0.0005_500.RData")


stopCluster(cl)
mpi.quit()
